package Hsqls;

import java.util.ArrayList;

import Repository.*;
import TableModels.*;
import UnitOfWork.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HsqlUserRepository implements UserRepository, UnitOfWorkRepository{

	Connection conn;
	
	public HsqlUserRepository() {
		begin();
	}
	public void begin(){
		try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.db");         
            conn.setAutoCommit(false);
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	public void exit(){
		try {
			if (!conn.isClosed())
			conn.close();
			
		} catch (SQLException e) {		
			e.printStackTrace();
		}catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	@Override
	public User withId(int id) {	
		User user = null;
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT id, login, password FROM t_sys_users where id = %d", id);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	user = new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   );
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return user;	
	}

	@Override
	public ArrayList<User> allOnPage(PagingInfo page) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT id, login, password FROM t_sys_users";
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;	
	}

	@Override
	public void add(User entity) {
		String command = String.format("INSERT INTO t_sys_users(id, login, password) " +
	        	"VALUES('%d', '%s', '%s', %s)",
	        	entity.getId(), entity.getLogin(), entity.getPassword());
		executeCommand(command);
		
	}

	@Override
	public void delete(User entity) {
		String command = String.format("DELETE FROM t_sys_users WHERE id = %d ",	entity.getId() );
		executeCommand(command);
		
	}

	@Override
	public void modify(User entity) {
		String command = String.format("UPDATE t_sys_users set login=%s , password = %s WHERE id = %d" ,
				 entity.getLogin(), entity.getPassword(), entity.getId());
		executeCommand(command);
		
	}

	@Override
	public int count() {
		int tmp=0;
		try {
            Statement stmt = conn.createStatement();
            String query = "SELECT count(*) from t_sys_users";
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	tmp = rows.getInt("count");
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
		return tmp;
	}

	@Override
	public ArrayList<User> withLogin(String login) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT id, login, password FROM t_sys_users WHERE login=%s", login);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;	
	}

	@Override
	public ArrayList<User> withLoginAndPassword(String login, String password) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT id, login, password FROM t_sys_users WHERE login=%s and password = %s"
            		, login, password);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;
	}

	@Override
	public void setupPermissions(User user) {
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("UPDATE t_sys_roles SET roleId=%d WHERE userId=%d"
            		, 0, user.getId());
            executeCommand(query);
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

	@Override
	public void persistAdd(Entity entity) {
		entity.setEntityState(EntityState.New);
		
	}

	@Override
	public void persistDelete(Entity entity) {
		entity.setEntityState(EntityState.Deleted);
		
	}

	@Override
	public void persistUpdate(Entity entity) {
		entity.setEntityState(EntityState.Modified);
		
	}
	private void executeCommand(String command) {
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(command);
            stmt.close();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

}
	
