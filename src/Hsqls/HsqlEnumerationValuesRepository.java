package Hsqls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Repository.EnumerationValueRepository;
import Repository.PagingInfo;
import TableModels.Entity;
import TableModels.EntityState;
import TableModels.EnumerationValue;
import TableModels.User;
import UnitOfWork.UnitOfWorkRepository;
import UnitOfWork.UnitOfWork;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWork{

	Connection conn;
	@Override
	public EnumerationValue withId(int id) {
		EnumerationValue enumer=null;
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE id=%d", id);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	enumer = new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   );
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumer;
	}

	@Override
	public ArrayList<EnumerationValue> allOnPage(PagingInfo page) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations ");
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public void add(EnumerationValue entity) {
	       String command = String.format("INSERT INTO t_sys_enums(intKey, stringKey, value, enumerationName) " +
	        	"VALUES('%d', '%s', '%s', %s)",
	        	entity.getId(), entity.getStringKey(), entity.getValue(), entity.getEnumerationName());
		executeCommand(command);		
	}

	@Override
	public void delete(EnumerationValue entity) {
		String command = String.format("DELETE FROM t_sys_enums WHERE id = %d",
	        	entity.getId()     );
		executeCommand(command);		
	}

	@Override
	public void modify(EnumerationValue entity) {
		String command = String.format("UPDATE t_sys_enums set stringKey=%s, value=%s, enumerationName=%s WHERE id=%d",
	        	 entity.getStringKey(), entity.getValue(), entity.getEnumerationName(), entity.getId() );
		executeCommand(command);
		
	}

	@Override
	public int count() {
		int tmp=0;
		try {
            Statement stmt = conn.createStatement();
            String query = "SELECT count(*) from t_sys_enums";
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	tmp = rows.getInt("count");
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
		return tmp;
	}

	@Override
	public ArrayList<EnumerationValue> withName(String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE enumeration_name = %s", name);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public ArrayList<EnumerationValue> withIntKey(int key, String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE intKey = %d and enumeration_name = %s", key, name);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public ArrayList<EnumerationValue> withStringKey(String key, String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE stringKey = %d and enumeration_name = %s", key, name);
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public void saveChanges() {
		try {
			if (!conn.isClosed())
            conn.commit();       
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }catch (Exception ex) {
        	ex.printStackTrace();
        }			
	}

	@Override
	public void undo() {
		try {
			if (!conn.isClosed())
            conn.rollback();      
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }catch (Exception ex) {
        	ex.printStackTrace();
        }			
	}

	@Override
	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.New);		
	}

	@Override
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.Deleted);	
		
	}

	@Override
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.Modified);	
		
	}
	
	private void executeCommand(String command) {
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(command);
            stmt.close();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

}
