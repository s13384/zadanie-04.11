package Hsqls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Repository.EnumerationValueRepository;
import Repository.RepositoryCatalog;
import Repository.UserRepository;
import TableModels.EnumerationValue;
import TableModels.User;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection conn;
	
	@Override
	public EnumerationValueRepository enumerations() {
		EnumerationValueRepository repo = new HsqlEnumerationValuesRepository();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations";
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	repo.add(new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return repo;
	}

	@Override
	public UserRepository users() {
			UserRepository repo = new HsqlUserRepository();
		
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT id, login, password FROM t_sys_users";
            ResultSet rows = stmt.executeQuery(query);
            
            while(rows.next()){
            	repo.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
            
            stmt.close();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return repo;
	}

}
